import React from "react";

const CartItem = props => {
    const { cartItem, cartKey } = props;

    const { product, amount } = cartItem;
    return (
        <div className=" column">
            <div className="box">
                <div className="media">
                    <div className="media-left">
                        <figure className="image is-64x64">
                            <img
                                src="https://skibikemarathon.com/wp-content/uploads/woocommerce-placeholder.png"
                                alt={product.shortDesc}
                            />
                        </figure>
                    </div>
                    <div className="media-content">
                        <b style={{ textTransform: "capitalize" }}>
                            {product.name}{" "}

                        </b>
                        <div>{product.shortDesc}</div>
                        <small>{`${amount} in cart`}</small>
                        <span className="tag">${product.price}</span>
                    </div>
                    <div
                        className="media-right"
                        onClick={() => props.removeFromCart(cartKey)}
                    >
                        <span className="delete is-large"></span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CartItem;
