import React from "react";
import withContext from "../withContext";

const Success = () => {
    return (
        <>
            <div className="hero is-medium is-link">
                <div className="hero-body container">
                    <h4 className="title">Success</h4>
                </div>
            </div>
            <br />
            <div className="container">
                <div className="column">
                    <div className="title has-text-grey-light">Successfully completed the order</div>
                </div>
            </div>
        </>
    );
};

export default withContext(Success);