import React, {Component} from "react";
import {Switch, Route, Link, BrowserRouter as Router} from "react-router-dom";
import axios from 'axios';

import Cart from './components/Cart';
import ProductList from './components/ProductList';
import Success from './components/Success';

import Context from "./Context";

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cart: {},
            products: []
        };
        this.routerRef = React.createRef();
    }

    async componentDidMount() {
        let cart = localStorage.getItem("cart");

        const products = await axios.get('./productsData.json');
        cart = cart? JSON.parse(cart) : {};
        this.setState({ products: products.data.products, cart });
    }

    addToCart = cartItem => {
        let cart = this.state.cart;
        if (cart[cartItem.id]) {
            cart[cartItem.id].amount += cartItem.amount;
        } else {
            cart[cartItem.id] = cartItem;
        }
        if (cart[cartItem.id].amount > cart[cartItem.id].product.stock) {
            cart[cartItem.id].amount = cart[cartItem.id].product.stock;
        }
        localStorage.setItem("cart", JSON.stringify(cart));
        this.setState({ cart });
    };

    deleteFromCart = cartItemId => {
        let cart = this.state.cart;
        delete cart[cartItemId];
        localStorage.setItem("cart", JSON.stringify(cart));
        this.setState({ cart });
    };

    emptyCart = () => {
        let cart = {};
        localStorage.removeItem("cart");
        this.setState({ cart });
    };

    checkout = () => {
        return 'Successfully completed the order';
    }

    render() {
        return (
            <Context.Provider
                value={{
                    ...this.state,
                    deleteFromCart: this.deleteFromCart,
                    addToCart: this.addToCart,
                    emptyCart: this.emptyCart,
                    checkout: this.checkout
                }}
            >
                <Router ref={this.routerRef}>
                    <div className="App">
                        <nav
                            className="navbar container"
                            role="navigation"
                            aria-label="main navigation"
                        >
                            <div className="navbar-brand">
                                <b className="navbar-item is-size-4 ">Riders</b>
                                <label
                                    role="button"
                                    className="navbar-burger burger"
                                    aria-label="menu"
                                    aria-expanded="false"
                                    data-target="navbarBasicExample"
                                    onClick={e => {
                                        e.preventDefault();
                                        this.setState({showMenu: !this.state.showMenu});
                                    }}
                                >
                                    <span aria-hidden="true"></span>
                                    <span aria-hidden="true"></span>
                                    <span aria-hidden="true"></span>
                                </label>
                            </div>
                            <div className={`navbar-menu ${
                                this.state.showMenu ? "is-active" : ""
                            }`}>
                                <Link to="/products" className="navbar-item">
                                    Products
                                </Link>
                                <Link to="/cart" className="navbar-item">
                                    Cart
                                    <span
                                        className="tag is-primary"
                                        style={{marginLeft: "5px"}}
                                    >
                    {Object.keys(this.state.cart).length}
                  </span>
                                </Link>
                            </div>
                        </nav>
                        <Switch>
                            <Route exact path="/" component={ProductList}/>
                            <Route exact path="/products" component={ProductList}/>
                            <Route exact path="/cart" component={Cart}/>
                            <Route exact path="/success" component={Success}/>
                        </Switch>
                    </div>
                </Router>
            </Context.Provider>
        );
    }
}
